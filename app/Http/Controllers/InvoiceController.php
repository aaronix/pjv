<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Purchase;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
        $this->middleware(function ($request, $next) {
            $this->user= Auth::user();
            if (Auth::user()->role_id==User::ADMIN) {
                return $next($request);
            }else {
                return redirect('/');
            }            
        });
        
        
        $this->data = [
            'invoice_index_active' => 'active',
            'section_header' => 'Facturas',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->data;
        $data['header'] = ['#', 'Cliente', 'Total', 'Impuesto'];
        $data['invoices'] = Invoice::cursor();
        return view('invoices.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $purchases = Purchase::where('invoice_id', 0)->get();
        foreach ($purchases->groupBy('user_id')->all() as $user_id => $purchaseDetail) {
            $products = $purchaseDetail->pluck('product')->groupBy('id')
                ->mapWithKeys(function ($details) {
                    $tax = ($details->first()->tax > 0) ? ($details->first()->price * (($details->first()->tax / 100))) : 0;
                    return [$details->first()->id => [
                        'product_id' => $details->first()->product_id,
                        'name' => $details->first()->name,
                        'qty' => $details->count(),
                        'price' => $details->first()->price,
                        'subtotal' => $details->first()->price * $details->count(),
                        'tax' => $tax * $details->count(),
                    ]];
                })
                ->all();

            $taxProduct = $purchaseDetail->pluck('product')
            ->map(function ($product) {
                $subtotal = ($product->tax > 0) ? ($product->price * (($product->tax / 100))) : 0;
                return $subtotal;
            })
                ->all();
            $tax = collect($taxProduct)->sum();
            
            $invoiceDetail[$user_id] = [
                'user_id' => $user_id,
                'products' => $products,
                'total' => collect($products)->sum('subtotal'),
                'tax' => collect($products)->sum('tax'),
                'subtotal' => collect($products)->sum('subtotal') - collect($products)->sum('tax'),
            ];

            $userInvoice = User::find($user_id);            
            
            $invoice = $userInvoice->invoices()->create([
                'total' => collect($products)->sum('subtotal'),
                'tax' => collect($products)->sum('tax'),
                'subtotal' => collect($products)->sum('subtotal') - collect($products)->sum('tax'),
            ]);
            // foreach ($products as $key => $product) {                
            //     $invoice->invoice_details()->create($purchases);
            // }
            foreach ($purchaseDetail as $key => $purchase) {                
                $invoice->invoice_details()->create(['purchase_id'=>$purchase->id]);
                $purchase->update(['invoice_id'=>$invoice->id]);
            }
            
            
        }
        return redirect()->route('invoices.index')
            ->with('status', 'success')
            ->with('msg', 'Facturas Creadas');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        $data = $this->data;
        $data['header'] = ['# Compra','Fecha de Compra', 'Nombre Producto', 'Precio', 'Impuesto'];
        $data['invoice_details'] = $invoice->invoice_details;
        $data['invoice'] = $invoice;

        return view('invoices.details', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        //
    }
}
