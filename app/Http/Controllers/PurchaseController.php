<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Purchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PurchaseController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->data = [
            'purchase_index_active' => 'active',
            'section_header' => 'Compras',
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->data;
        $data['header'] = ['#', 'Cliente', 'Producto', 'Total', 'Factura'];
        $data['purchases'] = Purchase::cursor();
        return view('purchases.index', compact('data'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = $this->data;
        $data['store'] = route('purchases.store');
        $data['products'] = Product::cursor();
        return view('purchases.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->input();
            $user =Auth::user();
            $product = Product::find($input['product_id']);

            $purchase = Purchase::create([
                'product_id'=>$input['product_id'],
                'user_id'=>$user->id,
                'total'=>$product->price,
            ]);

            $status = 'success';
            $msg = "¡La Compra <b>{$purchase->id}</b> se ha creado exitosamente!";
        } catch (\Throwable $th) {
            report($th);
            $status = 'danger';
            $msg = '¡No se pudo crear la compra!';
        }
        return redirect()->route('purchases.index')
            ->with('status', $status)
            ->with('msg', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show(Purchase $purchase)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        //
    }
}
