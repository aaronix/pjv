<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            if (Auth::user()->role_id == User::ADMIN) {
                return $next($request);
            } else {
                return redirect('/');
            }
        });
        $this->data = [
            'catalog_dropdown_active' => 'active',
            'product_index_active' => 'active',
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->data;
        $data['section_header'] = 'Productos';
        $data['header'] = ['#', 'Nombre', 'Precio', 'Impuesto', 'Precio sin Impuesto', 'Opciones'];
        $data['products'] = Product::cursor();
        return view('products.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = $this->data;
        $data['section_header'] = 'Productos';
        $data['store'] = route('products.store');
        return view('products.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->input();
            $product = Product::create($input);

            $status = 'success';
            $msg = "¡El Producto <b>{$product->id} .- {$product->name}</b> se ha creado exitosamente!";
        } catch (\Throwable $th) {
            report($th);
            $status = 'danger';
            $msg = '¡No se pudo crear el producto!';
        }
        return redirect()->route('products.index')
            ->with('status', $status)
            ->with('msg', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $data = $this->data;
        $data['section_header'] = 'Productos';
        $data['edit'] = route('products.edit', ['product' => $product]);
        $data['product'] = $product;
        return view('products.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $data = $this->data;
        $data['section_header'] = 'Productos';
        $data['update'] = route('products.update', ['product' => $product]);
        $data['product'] = $product;
        return view('products.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        try {
            $input = $request->input();
            $product->update([
                'name' => $input['name'],
                'price' => $input['price'],
                'tax' => $input['tax'],
            ]);

            $status = 'success';
            $msg = "¡El Producto <b>{$product->id} .- {$product->name}</b> se ha actualizado exitosamente!";
        } catch (\Throwable $th) {
            report($th);
            $status = 'danger';
            $msg = '¡No se pudo actualizar el producto!';
        }
        return redirect()->route('products.index')
            ->with('status', $status)
            ->with('msg', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        try {
            if ($product->purchases->count()) {
                $status = 'danger';
                $msg = '¡No se pudo eliminar el producto!';    
            }else {                
                $product->delete();
                $status = 'success';
                $msg = "¡El Producto <b>{$product->id} .- {$product->name}</b> se ha eliminado exitosamente!";
            }
        } catch (\Throwable $th) {
            report($th);
            $status = 'danger';
            $msg = '¡No se pudo eliminar el producto!';
        }
        return redirect()->route('products.index')
            ->with('status', $status)
            ->with('msg', $msg);
    }
}
