<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceDetail extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        'invoice_id',
        'user_id',
        'purchase_id',
        'name',
        'qty',
        'subtotal',
        'tax',
        'price',
    ];

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function purchase()
    {
        return $this->belongsTo(Purchase::class);
    }
}
