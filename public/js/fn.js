function deleteRow(genre, model, deleteLink, table) {
    // console.log('deleteRow');
    
    let name
    let uuid
    
    if (genre == 'la') {
        confirmTextGenre = 'Borrala'
        deleteOKText = 'Borrada'
    }else{
        confirmTextGenre = 'Borralo'
        deleteOKText = 'Borrado'
    }
    $('#table-index tbody').on('click', '.delete', function (e) {
                
        name = $(this).data('name')
        uuid = $(this).data('uuid')
        swal({
            title: '¿Quieres Borrar '+genre+' '+model+' '+name+'?',
            text: "",
            icon: 'warning',
            buttons:true,
            buttons: {
                confirm: {
                    text: 'Sí, ¡'+confirmTextGenre+'!',
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                },
                cancel: {
                    text: "Cancelar",
                    value: false,
                    visible: true,
                    className: "",
                    closeModal: true,
                },                        
            },
            // showCancelButton: true,
            // confirmButtonColor: '#3085d6',
            // cancelButtonColor: '#d33',
            // cancelButtonText: 'Cancelar', 
            // confirmButtonText: 'Sí, ¡'+confirmTextGenre+'!'
        }).then((result) => {                   
            if (result) {
                axios.post('/'+deleteLink+'/'+uuid, {
                _method: 'DELETE'
                })
                .then( response => {
                    // console.log(response);
                    deleteOK = response.data.results
                    if (deleteOK.delete || (deleteOK===true)) {
                        var elem = document.createElement("div");
                        elem.innerHTML = genre.charAt(0).toUpperCase() + genre.slice(1) +' '+model+' <b>' +name+'</b> se ha Borrado';
                        swal({
                            title: '¡'+deleteOKText+'!',
                            content: elem,
                            icon: 'success',
                        })
                        table.ajax.reload();    
                    }else{

                        var elem = document.createElement("div");
                        elem.innerHTML = (deleteOK.msg) ? deleteOK.msg : genre.charAt(0).toUpperCase() + genre.slice(1) +' '+model+' ' +name+' no se ha Borrado';
                        swal({
                            title: '¡No se pudo Borrar!',
                            content: elem,
                            icon: 'error',
                        })
                    }                        
                })
                .catch( error => {
                    swal({
                        title: '¡No se pudo Borrar!',
                        content: genre.charAt(0).toUpperCase() + genre.slice(1) +' '+model+' ' +name+' no se ha Borrado',
                        icon: 'error',
                    })
                    console.log(error);
                    //handle failure
                })
            }
        })    
    })
}


$('.slide-btn-menu').addClass('invisible'); //FIRST TIME INVISIBLE

// ADD SLIDEDOWN ANIMATION TO slide-btn-menu
$('.dropdown').on('show.bs.dropdown', function(e){
    console.log('show');
    $('.slide-btn-menu').removeClass('invisible');
    $(this).find('.slide-btn-menu').first().stop(true, true).slideDown();
});

// ADD SLIDEUP ANIMATION TO slide-btn-menu
$('.dropdown').on('hide.bs.dropdown', function(e){
    console.log('hide');
    $(this).find('.slide-btn-menu').first().stop(true, true).slideUp();
  });