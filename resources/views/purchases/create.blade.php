@extends('layout-default')
@push('css-libraries')
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('modules/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('modules/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('modules/datatables.net-select-bs4/css/select.bootstrap4.min.css') }}">
@endpush
@section('header')
    <a href="{{ route('purchases.index') }}" class="btn btn-primary btn-sm save">Regresar</a>
@endsection
@section('body')
    {{-- <h2 class="section-title">This is Example Page</h2>
<p class="section-lead">This page is just an example for you to create your own page.</p> --}}
    <div class="card">
        <form action="{{ $data['store'] }}" class="needs-validation" method="post" id="form">
            @csrf
            <div class="card-header">
                <h4>Compras</h4>
            </div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-{{ session('status') }} alert-dismissible show fade">
                        <div class="alert-body">
                            <button class="close" data-dismiss="alert">
                                <span>&times;</span>
                            </button>
                            {!! session('msg') !!}
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row form-group-sm">
                            <label class="col-sm-3 col-form-label">Nombre del producto <span
                                    class="@error('product_id') text-danger @enderror">*</span> </label>
                            <div class="col-sm-9">
                                <div id="product_id_div" class="col-sm-9 @error('product_id') is-invalid @enderror">
                                    <select
                                        class="form-control form-control-sm  @error('product_id') is-invalid @enderror select2"
                                        name="product_id" id="product_id">
                                        <option value="">-- Seleccionar --</option>
                                        @foreach ($data['products'] as $product)
                                            @if ((old('product_id') ?? 0) == $product->id)
                                                <option selected value="{{ $product->id }}">{{ $product->name }}
                                                </option>
                                            @endif
                                            <option data-price={{ round($product->price,2) }} 
                                                data-tax={{ round($product->tax,2) }}
                                                data-pricenotax={{ round( ($product->tax>0)?($product->price/(1+($product->tax/100))):0,2) }}
                                                value="{{ $product->id }}">{{ $product->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">
                                        @error('product_id')
                                            {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row form-group-sm">
                            <label class="col-sm-3 col-form-label">Precio (con impuesto) <span
                                    class="@error('price') text-danger @enderror">*</span> </label>
                            <div class="col-sm-9">
                                <label class="form-control form-control-sm" for="" id="price">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row form-group-sm">
                            <label class="col-sm-3 col-form-label">Impuesto <span
                                    class="@error('tax') text-danger @enderror">*</span> </label>
                            <div class="col-sm-9">
                                <label class="form-control form-control-sm" for="" id="tax">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row form-group-sm">
                            <label class="col-sm-3 col-form-label">Precio sin Impuesto <span
                                    class="@error('tax') text-danger @enderror">*</span> </label>
                            <div class="col-sm-9">
                                <label class="form-control form-control-sm" for="" id="pricenotax">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="card-footer bg-whitesmoke">
                <button type="submit" class="btn btn-primary btn-sm save">Guardar</button>
            </div>
        </form>
    </div>
@endsection

@push('js-libraries')
    <!-- JS Libraies -->
    <script src="{{ asset('modules/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('modules/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('modules/datatables.net-select-bs4/js/select.bootstrap4.min.js') }}"></script>

@endpush

@push('js')

    <script type="text/javascript">
        $(document).ready(function() {
            $('#product_id').on('select2:select', function(e) {                
                var data = e.params.data;
                console.log(data.id);
                if (data.id>0) {
                    
                    price = $("#product_id").select2().find(":selected").data("price");
                    $("#price").html(price)
                    tax = $("#product_id").select2().find(":selected").data("tax");
                    $("#tax").html(tax)
                    pricenotax = $("#product_id").select2().find(":selected").data("pricenotax");
                    $("#pricenotax").html(pricenotax)
                }else{
                    $("#price").empty()
                    $("#tax").empty()
                    $("#pricenotax").empty()
                }
                
                
                // priceId = $(this).closest('tr').find('.prices').attr('id')
                
            });
        });
    </script>

@endpush
