@extends('layout-default')
@push('css-libraries')
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('modules/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('modules/datatables.net-select-bs4/css/select.bootstrap4.min.css') }}">
@endpush
@section('header')
    <a href="{{ route('invoices.index') }}" class="btn btn-primary btn-sm save">Regresar</a>
@endsection
@section('body')
    {{-- <h2 class="section-title">This is Example Page</h2>
<p class="section-lead">This page is just an example for you to create your own page.</p> --}}

    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                    <i class="far fa-user"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Factura</h4>
                    </div>
                    <div class="card-body">
                        #{{ $data['invoice']->id }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-danger">
                    <i class="far fa-newspaper"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Cliente</h4>
                    </div>
                    <div class="card-body">
                        {{ $data['invoice']->user->name }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-warning">
                    <i class="far fa-file"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Monto Total</h4>
                    </div>
                    <div class="card-body">
                        $ {{ number_format($data['invoice']->total, 2, '.', ', ') }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                    <i class="fas fa-circle"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Impuesto Total</h4>
                    </div>
                    <div class="card-body">
                        $ {{ number_format($data['invoice']->tax, 2, '.', ', ') }}
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="card-header">
        <h4>Compras</h4>
    </div>
    <div class="card">

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-{{ session('status') }} alert-dismissible show fade">
                    <div class="alert-body">
                        <button class="close" data-dismiss="alert">
                            <span>&times;</span>
                        </button>
                        {!! session('msg') !!}
                    </div>
                </div>
            @endif
            <div class="table-responsive">
                <table class="table table-striped" id="table-index">
                    <thead>
                        <tr>
                            @foreach ($data['header'] as $item)
                                <th id="{{ $item }}" class="text-right">{{ $item }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['invoice_details'] as $invoice_detail)
                            <tr>
                                <td class="text-right">
                                    <a href="{{ route('purchases.show', ['purchase' => $invoice_detail->purchase]) }}">
                                        {{ $invoice_detail->purchase->id }}
                                    </a>
                                </td>
                                <td class="text-right">
                                    {{ $invoice_detail->purchase->created_at }}
                                </td>
                                <td class="text-right">
                                    {{ $invoice_detail->purchase->product->name }}
                                </td>
                                <td class="text-right">$
                                    {{ number_format($invoice_detail->purchase->product->price, 2, '.', ', ') }}
                                </td>
                                <td class="text-right">$
                                    {{ $invoice_detail->purchase->product->tax > 0 ? number_format($invoice_detail->purchase->product->price * ($invoice_detail->purchase->product->tax / 100), 2, '.', ', ') : '' }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            @foreach ($data['header'] as $item)
                                <th class="text-right"> {{ $item }}</th>
                            @endforeach
                        </tr>
                    </tfoot>
                </table>
            </div>

        </div>
        <div class="card-footer bg-whitesmoke">

        </div>
    </div>
@endsection

@push('js-libraries')


    <!-- JS Libraies -->
    <script src="{{ asset('modules/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('modules/datatables.net-select-bs4/js/select.bootstrap4.min.js') }}"></script>


@endpush

@push('js')

    <script type="text/javascript">
        $(document).ready(function() {

            var table = $('#table-index').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
                },
                "order": [
                    [1, "asc"]
                ],
            });
        });
    </script>

@endpush
