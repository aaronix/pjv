@extends('layout-default')
@push('css-libraries')
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('modules/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('modules/datatables.net-select-bs4/css/select.bootstrap4.min.css') }}">
@endpush
@section('header')
    <a href="{{ route('invoices.create') }}" class="btn btn-primary btn-sm save">Facturar</a>
@endsection
@section('body')
    {{-- <h2 class="section-title">This is Example Page</h2>
<p class="section-lead">This page is just an example for you to create your own page.</p> --}}
    <div class="card">
        <div class="card-header">
            <h4>Facturas</h4>
        </div>
        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-{{ session('status') }} alert-dismissible show fade">
                    <div class="alert-body">
                        <button class="close" data-dismiss="alert">
                            <span>&times;</span>
                        </button>
                        {!! session('msg') !!}
                    </div>
                </div>
            @endif
            <div class="table-responsive">
                <table class="table table-striped" id="table-index">
                    <thead>
                        <tr>
                            @foreach ($data['header'] as $item)
                                <th id="{{ $item }}" class="text-right">{{ $item }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['invoices'] as $invoice)
                            <tr>
                                <td class="text-right">
                                    <a href="{{ route('invoices.show', ['invoice' => $invoice]) }}"> 
                                        {{ $invoice->id }}
                                    </a>
                                </td>
                                <td class="text-right">
                                    {{ $invoice->user->name }}
                                </td>
                                <td class="text-right">$
                                    {{ number_format($invoice->total, 2, '.', ', ') }}
                                </td>
                                <td class="text-right">$
                                    {{ number_format($invoice->tax, 2, '.', ', ') }}
                                </td>                                
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            @foreach ($data['header'] as $item)
                                <th class="text-right"> {{ $item }}</th>
                            @endforeach
                        </tr>
                    </tfoot>
                </table>
            </div>

        </div>
        <div class="card-footer bg-whitesmoke">

        </div>
    </div>
@endsection

@push('js-libraries')


    <!-- JS Libraies -->
    <script src="{{ asset('modules/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('modules/datatables.net-select-bs4/js/select.bootstrap4.min.js') }}"></script>


@endpush

@push('js')

    <script type="text/javascript">
        $(document).ready(function() {

            var table = $('#table-index').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
                },
                
            });
        });
    </script>

@endpush
