@extends('layout-default')
@section('header')
    <a href="{{ $data['edit'] }}" class="btn btn-primary btn-sm save">Editar</a>
@endsection
@section('body')
    {{-- <h2 class="section-title">This is Example Page</h2>
<p class="section-lead">This page is just an example for you to create your own page.</p> --}}
    <div class="card">

        <div class="card-header">
            <h4>Productos</h4>
        </div>
        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-{{ session('status') }} alert-dismissible show fade">
                    <div class="alert-body">
                        <button class="close" data-dismiss="alert">
                            <span>&times;</span>
                        </button>
                        {!! session('msg') !!}
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row form-group-sm">
                        <label class="col-sm-3 col-form-label">Nombre del producto <span
                                class="@error('name') text-danger @enderror">*</span> </label>
                        <div class="col-sm-9">
                            <label class="form-control form-control-sm" for="">
                                {{ $data['product']->name ?? '' }}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row form-group-sm">
                        <label class="col-sm-3 col-form-label">Precio (con impuesto) <span
                                class="@error('price') text-danger @enderror">*</span> </label>
                        <div class="col-sm-9">
                            <label class="form-control form-control-sm" for="">
                                $ {{ number_format($data['product']->price ?? 0, 2, '.', ', ') }}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row form-group-sm">
                        <label class="col-sm-3 col-form-label">Impuesto <span
                                class="@error('tax') text-danger @enderror">*</span> </label>
                        <div class="col-sm-9">
                            <label class="form-control form-control-sm" for="">
                                {{ round($data['product']->tax ?? 0, 2) }}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row form-group-sm">
                        <label class="col-sm-3 col-form-label">Precio sin Impuesto <span
                                class="@error('tax') text-danger @enderror">*</span> </label>
                        <div class="col-sm-9">
                            <label class="form-control form-control-sm" for="">
                                $ {{ $data['product']->tax > 0 ? number_format($data['product']->price / $data['product']->tax, 2, '.', ', ') : '' }}                                
                            </label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="card-footer bg-whitesmoke">
            <a href="{{ $data['edit'] }}" class="btn btn-primary btn-sm save">Editar</a>
        </div>
    </div>
@endsection
