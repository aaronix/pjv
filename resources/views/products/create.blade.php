@extends('layout-default')
@push('css-libraries')    
<!-- CSS Libraries -->
<link rel="stylesheet" href="{{asset('modules/bootstrap-daterangepicker/daterangepicker.css')}}">
<link rel="stylesheet" href="{{asset('modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('modules/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">
@endpush
@section('header')
    <a href="{{ route('products.index') }}" class="btn btn-primary btn-sm save">Regresar</a>
@endsection
@section('body')
{{-- <h2 class="section-title">This is Example Page</h2>
<p class="section-lead">This page is just an example for you to create your own page.</p> --}}
<div class="card">
    <form action="{{ $data['store'] }}" class="needs-validation" method="post" id="form">
        @csrf
    <div class="card-header">
        <h4>Productos</h4>
    </div>
    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-{{ session('status') }} alert-dismissible show fade">
                <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>&times;</span>
                </button>
                {!! session('msg') !!}
                </div>
            </div>
        @endif
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group row form-group-sm">
                    <label class="col-sm-3 col-form-label">Nombre del producto <span class="@error('name') text-danger @enderror">*</span> </label>
                    <div class="col-sm-9">
                        <input type="text" name="name" id="name" required autocomplete="false" placeholder=""
                        @if(old('name'))
                            value="{{ old('name') }}"
                        @endif
                        class="form-control form-control-sm  @error('name') is-invalid @enderror">
                        <div class="invalid-feedback">
                            @error('name')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group row form-group-sm">
                    <label class="col-sm-3 col-form-label">Precio (con impuesto) <span class="@error('price') text-danger @enderror">*</span> </label>
                    <div class="col-sm-9">
                        <input type="number" min="0" step="any" name="price" id="price" autocomplete="false" placeholder=""
                        @if(old('price'))
                            value="{{ old('price') }}"
                        @endif
                        class="form-control form-control-sm  @error('price') is-invalid @enderror">
                        <div class="invalid-feedback">
                            @error('price')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group row form-group-sm">
                    <label class="col-sm-3 col-form-label">Impuesto <span class="@error('tax') text-danger @enderror">*</span> </label>
                    <div class="col-sm-9">
                        <input type="number" min="0" step="any" name="tax" id="tax" autocomplete="false" placeholder=""
                        @if(old('tax'))
                            value="{{ old('tax') }}"
                        @endif
                        class="form-control form-control-sm  @error('tax') is-invalid @enderror">
                        <div class="invalid-feedback">
                            @error('tax')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <div class="card-footer bg-whitesmoke">
        <button type="submit" class="btn btn-primary btn-sm save">Guardar</button>
    </div>
</form>
</div>
@endsection

@push('js-libraries')


<!-- JS Libraies -->
<script src="{{asset('modules/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('modules/datatables.net-select-bs4/js/select.bootstrap4.min.js')}}"></script>


@endpush

@push('js')
    
<script type="text/javascript" >
    $(document).ready(function() {

        var table = $('#table-index').DataTable({
            "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
                },
        });
    });
</script>

@endpush
