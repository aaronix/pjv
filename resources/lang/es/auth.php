<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Las credenciales no coinciden con nuestros registros.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Demasiados intentos de entrar. Por favor intente en :seconds segundos.',

];
