
## Crear la base de datos

CREATE DATABASE `p0d3rjUd1c14L`;

## Para iniciar el sistema
1.-Ejecuta

git clone https://aaronix@bitbucket.org/aaronix/pjv.git

2.-Ejecuta

composer install

3.-Copia el .env, y llena los datos de tu base de datos

cp .env.example .env ó copia .env.example .env

4.-Ejecuta

php artisan key:generate

5.-Crea una base de datos, llena los datos en el .env y ejecuta

php artisan migrate --seed

6.-Ejecuta

php artisan serve

7.-Para entrar al sistema el usuario Administrador es

Usuario:

admin@admin.com

Password:

12345678

y el usuario Cliente 

Usuario:

cliente@cliente.com

Password:

12345678

y el usuario Cliente Dos 

Usuario:

clienteDOS@clienteDOS.com

Password:

12345678

