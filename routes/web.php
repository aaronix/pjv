<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PurchaseController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// })->name('welcome');
Route::get('/',[HomeController::class, 'dash'])->name('welcome');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

// Route::get('/dash', [HomeController::class, 'dash'])->name('dash');

Route::get('/productos', [ProductController::class, 'index'])->name('products.index');
Route::get('/productos/nuevo', [ProductController::class, 'create'])->name('products.create');
Route::get('/productos/{product}', [ProductController::class, 'show'])->name('products.show');
Route::get('/productos/{product}/editar', [ProductController::class, 'edit'])->name('products.edit');
Route::post('/productos', [ProductController::class, 'store'])->name('products.store');
Route::put('/productos/{product}', [ProductController::class, 'update'])->name('products.update');
Route::get('/productos/{product}/borrar', [ProductController::class, 'destroy'])->name('products.destroy');


Route::get('/compras', [PurchaseController::class, 'index'])->name('purchases.index');
Route::get('/compras/nuevo', [PurchaseController::class, 'create'])->name('purchases.create');
Route::get('/compras/{purchase}', [PurchaseController::class, 'show'])->name('purchases.show');
Route::get('/compras/{purchase}/editar', [PurchaseController::class, 'edit'])->name('purchases.edit');
Route::post('/compras', [PurchaseController::class, 'store'])->name('purchases.store');

Route::get('/clientes', [CustomerController::class, 'index'])->name('customers.index');

Route::get('/facturas', [InvoiceController::class, 'index'])->name('invoices.index');
Route::get('/facturas/nuevo', [InvoiceController::class, 'create'])->name('invoices.create');
Route::get('/facturas/{invoice}', [InvoiceController::class, 'show'])->name('invoices.show');