<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_details', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('invoice_id')->default(0);
            $table->bigInteger('user_id')->default(0);
            $table->bigInteger('purchase_id')->default(0);
            $table->string('name')->nullable();
            $table->integer('qty')->default(0);
            $table->decimal('subtotal',16,4)->default(0);
            $table->decimal('tax',16,4)->default(0);
            $table->decimal('price',16,4)->default(0);
            
            $table->softDeletes();
            
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_details');
    }
}
