<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (User::where('role_id', 1)->count() == 0) {
            $user_admin = User::create([
                'name'      => 'admin',
                'email'     => 'admin@admin.com',
                'password'  => Hash::make('12345678'),
                'role_id'   =>  User::ADMIN
            ]);
        }
        if (User::where('email', 'cliente@cliente.com')->count() == 0) {
            $user_customer = User::create([
                'name'      => 'cliente',
                'email'     => 'cliente@cliente.com',
                'password'  => Hash::make('12345678'),
            ]);
        }
        if (User::where('email', 'clienteDOS@clienteDOS.com')->count() == 0) {
            $user_customer = User::create([
                'name'      => 'cliente DOS',
                'email'     => 'clienteDOS@clienteDOS.com',
                'password'  => Hash::make('12345678'),
            ]);
        }
    }
}
